#!/usr/bin/env python3
import argparse
import logging
import re
import settings as sts

from logging.config import dictConfig
from pathlib import Path
from pytube import YouTube, Playlist, Channel

MAX_RETRIES = 3
DEFAULT_DOWNLOAD = Path("download")
if not sts.LOG_FILE.parent.exists():
    sts.LOG_FILE.parent.mkdir(parents=True, exist_ok=True)
dictConfig(sts.LOGGING_CONFIG)


def download_video(link: str, target_download: Path):
    youtube_object = YouTube(link)
    video = youtube_object.streams.get_highest_resolution()
    retries = 0
    try:
        logging.info(f"Downloading: {video.title}")
        video.download(str(target_download), max_retries=MAX_RETRIES)
    except Exception as e:
        logging.error(f"error downloading: {e}")
        return
    logging.info("Download is completed successfully")


def download_channel(link: str, target_download: Path):
    main_obj = YouTube(link)
    channel = Channel(main_obj.channel_url)
    play_list = Playlist(channel.video_urls)
    dnl_folder = target_download / channel.channel_name
    logging.info(f"Downloading from channel: {channel.channel_name} to {str(dnl_folder)}")
    video_count = 0
    for video in channel.videos:
        video_count += 1
        logging.info(f"Downloading [{video_count}/{channel.length}]: {video.title}")
        try:
            video.streams.get_highest_resolution().download(str(dnl_folder), max_retries=MAX_RETRIES)
        except Exception as e:
            logging.error(f"error downloading: {e}")
    logging.info("Download is completed")


def download_playlist(link: str, target_download: Path):
    playlist = Playlist(link)
    dnl_folder = target_download / playlist.title
    logging.info(f"Downloading from playlist: {playlist.title} to {str(dnl_folder)}")
    video_count = 0
    for video in playlist.videos:
        video_count += 1
        logging.info(f"Downloading [{video_count}/{playlist.length}]: {video.title}")
        try:
            video.streams.get_highest_resolution().download(str(dnl_folder), max_retries=MAX_RETRIES)
        except Exception as e:
            logging.error(f"error downloading: {e}")
    logging.info(f"Download is completed to {dnl_folder}")


def main(params):
    target_download = DEFAULT_DOWNLOAD
    if params.download:
        target_download = Path(params.download)
    elif not DEFAULT_DOWNLOAD.exists():
        DEFAULT_DOWNLOAD.mkdir(parents=True, exist_ok=True)
    match = re.search("\/playlist\?list", params.link)
    if match:
        download_playlist(params.link, target_download)
    else:
        download_video(params.link, target_download)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Video Downloader")
    parser.add_argument("-l", "--link",
                        dest="link", type=str, required=False,
                        help="Single URL to a video to download"
                        )
    parser.add_argument("-t", "--targetfolder",
                        dest="download", type=str, required=False,
                        help="Target folder to store downloads. Must be relative to current download folder."
                        )
    # parser.add_argument("-a", "--all",
    #                     dest="all", required=False,
    #                     action="store_true", default=False,
    #                     help="Download all videos from channel. Use any video link from a channel."
    #                     )

    params = parser.parse_args()
    main(params)
